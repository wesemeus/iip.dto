﻿using IIP.Dto.Requests.CreateShortUrl;

namespace IIP.Dto.Producers;

public interface ICreateShortUrlProducer
{
    Task<CreateShortUrlResponse> RequestAsync(CreateShortUrlRequest request);
}