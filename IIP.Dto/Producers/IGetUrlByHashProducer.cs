﻿using IIP.Dto.Requests.GetUrlByHash;

namespace IIP.Dto.Producers;

public interface IGetUrlByHashProducer
{
    Task<GetUrlByHashResponse> RequestAsync(GetUrlByHashRequest request);
}