﻿using IIP.Dto.Requests.GetUrlFromCache;

namespace IIP.Dto.Producers;

public interface IGetUrlFromCacheProducer
{
    Task<GetUrlFromCacheResponse> RequestAsync(GetUrlFromCacheRequest request);
}