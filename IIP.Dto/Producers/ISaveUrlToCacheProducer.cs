﻿using IIP.Dto.Requests.SaveUrlToCache;

namespace IIP.Dto.Producers;

public interface ISaveUrlToCacheProducer
{
    Task<SaveUrlToCacheResponse> RequestAsync(SaveUrlToCacheRequest request);
}