﻿namespace IIP.Dto.Requests.CreateShortUrl;

public sealed record CreateShortUrlRequest
{
    public string? Url { get; init; }
}