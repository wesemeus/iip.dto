﻿namespace IIP.Dto.Requests.CreateShortUrl;

public sealed record CreateShortUrlResponse
{
    public string ShortUrl { get; init; }
}