﻿namespace IIP.Dto.Requests.GetUrlByHash;

public sealed record GetUrlByHashRequest
{
    public string? Hash { get; set; }
}