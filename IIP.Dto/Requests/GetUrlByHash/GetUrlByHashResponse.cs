﻿namespace IIP.Dto.Requests.GetUrlByHash;

public sealed record GetUrlByHashResponse
{
    public string? Url { get; set; }
}