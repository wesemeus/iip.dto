﻿namespace IIP.Dto.Requests.GetUrlFromCache;

public sealed record GetUrlFromCacheRequest
{
    public string? Hash { get; set; }
}