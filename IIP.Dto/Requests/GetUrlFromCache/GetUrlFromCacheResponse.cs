﻿namespace IIP.Dto.Requests.GetUrlFromCache;

public sealed record GetUrlFromCacheResponse
{
    public string? Url { get; set; }
}