﻿namespace IIP.Dto.Requests.SaveUrlToCache;

public record SaveUrlToCacheRequest
{
    public string? Hash { get; set; }

    public string? Url { get; set; }
}