﻿namespace IIP.Dto.Requests.SaveUrlToCache;

public sealed record SaveUrlToCacheResponse
{
    public bool IsSuccess { get; set; }
}